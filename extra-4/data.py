#!/usr/bin/env python
# coding: utf-8

"""
>>> aa_milne_arr = ['pooh', 'rabbit', 'piglet', 'Christopher']
>>> np.random.choice(aa_milne_arr, 5, p=[0.5, 0.1, 0.1, 0.3])
"""
# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------

import math
import random
import numpy as np
def generate_data(n=100, seed=0):
	if seed==-1: seed = random.randint(0,2**31)
	random.seed(seed)
	np.random.seed(seed)

	x = np.linspace(0,np.pi,n)
	

	angle = np.random.uniform(0.05,0.1,size=(size,1))
	angle = angle*4*np.pi
	x1 = np.cos(angle)*angle
	x2 = np.sin(angle)*angle
	x = np.concatenate([x1,x2],axis=1)






# ------------------------------------------------------
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def test():

	"""
	fig = plt.figure(figsize=(5,5))
	plt.subplots_adjust(left=0, right=1, top=1, bottom=0)


	ax = fig.add_subplot(1, 1, 1)
	ax.axis('equal')

	n = 2**2
	for i,(dist,seed) in enumerate(zip(['Lx','Rx'],[23,7])):
		x = generate_data(dist, n, seed=seed)
		ax.scatter(x[:,0], x[:,1], s=60, color=cm.tab10(i), label=f'labeled:{i}', marker='d', zorder=10, linewidth=0.5, edgecolor='black')
	n = 2**10
	for i,dist in enumerate(['Lu','Ru']):
		x = generate_data(dist, n)
		ax.scatter(x[:,0], x[:,1], s=0.5, color=cm.tab10(i+2), label=f'unlabeled:{i}')

	ax.legend(fontsize=12,framealpha=1.0).set_zorder(-1)
	ax.axis('off')

	file_name = 'OUTPUTs/data-sample.png'
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	"""


# ------------------------------------------------------
if __name__=='__main__':
	test()





#