#!/usr/bin/env python
# coding: utf-8

# -------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------

import torch
def get_KLdiv(margin_q, margin_p):
	q		= torch.softmax(margin_q, dim=1)
	logq	= torch.log_softmax(margin_q, dim=1)
	logp	= torch.log_softmax(margin_p, dim=1)
	div		= q*(logq-logp)
	return div.sum(dim=1)

import torch.nn.functional as F
from torch.autograd import Variable
def get_Radv(config, model, u, pu):
	d = torch.randn(u.shape)	# 正規分布のベクトルの生成
	if 0<=config.core: d = d.cuda()
	for _ in range(config.power_iter):
		d = F.normalize(d, eps=config.norm_eps)
		d = Variable(d, requires_grad=True)
		padv	= model(u + config.xi * d)
		delta	= get_KLdiv(pu, padv).mean(dim=0) # データ方向にaverageする

		model.zero_grad()
		delta.backward()
		d = d.grad.data.clone() # 上でVariableするのでOKなハズ

	d = F.normalize(d, eps=config.norm_eps)
	d = Variable(d, requires_grad=False)
	return config.epsilon * d

# ------------------------------------------------------

import data as dataset
def get_data(config):
	def _get(size, salt):
		seed = config.data_seed+1
		seed = (seed*salt) % (10**9+7)
		return dataset.generate(size, seed, config.var, config.core)

	data = {}
	data['train']	= _get(config.train_size, 1)
	data['valid']	= _get(config.valid_size, 2)
	data['test']	= _get(config.test_size,  3)
	data['unlabeled'] = _get(config.unlabeled_size, 4)
	return data

# -------------------------------
import torch
import random
class Model(torch.nn.Module):
	def __init__(self, hidden_dim, layer_size):
		super(Model, self).__init__()
		d,L = hidden_dim, layer_size
		self.Li = torch.nn.Linear(2, d)
		self.Ls	= torch.nn.ModuleList([torch.nn.Linear(d,d) for _ in range(L)])
		self.Lo = torch.nn.Linear(d, 2)

	def forward(self, x):
		x = self.Li(x)
		for L in self.Ls:
			x = torch.relu(L(x))
		x = self.Lo(x)
		return x

import numpy as np
def get_model(config):
	random.seed(config.model_seed)
	np.random.seed(config.model_seed)
	torch.manual_seed(config.model_seed)
	model = Model(config.hidden_dim, config.layer_size)
	if 0<=config.core: model = model.cuda()
	return model

# ------------------------------------------------------
import torch.optim as optim
def optimize(model, data, config):
	optimizer	= optim.Adam(model.parameters(), lr=config.lr)
	loss_func	= torch.nn.CrossEntropyLoss()
	scheduler	= lambda epoch: 1 / (epoch*config.decay + 1)
	scheduler	= optim.lr_scheduler.LambdaLR(optimizer, lr_lambda = scheduler)

	def update():
		model.train()
		lp 			= model(data['train'].x)
		standard	= loss_func(lp, data['train'].y)

		up		= model(data['unlabeled'].x)
		d		= get_Radv(config, model, data['unlabeled'].x, up.detach())
		ap		= model(data['unlabeled'].x + d)
		adversarial	= get_KLdiv(up, ap).mean(dim=0)

		total = standard + config.alpha * adversarial

		optimizer.zero_grad()
		total.backward()
		optimizer.step()
		scheduler.step()

		measure = {}
		measure['loss']		= total.item()
		measure['loss-standard']	= standard.item()
		measure['loss-adversarial']	= adversarial.item()
		measure['loss-unlabeled']	= loss_func(up, data['unlabeled'].y).item()
		prediction	= torch.max(lp.data,1)[1]
		measure['accuracy'] 	= (prediction==data['train'].y).sum().item()/len(lp)
		prediction	= torch.max(up.data,1)[1]
		measure['accuracy-unlabel'] = (prediction==data['unlabeled'].y).sum().item()/len(up)
		return measure

	def evaluate(mode):
		model.eval()
		measure = {'loss':0, 'accuracy':0}
		with torch.no_grad():
			output		= model(data[mode].x)
			prediction	= torch.max(output.data, 1)[1]
			measure['loss'] 	+= loss_func(output, data[mode].y).item()
			measure['accuracy'] += (data[mode].y==prediction).sum().item()
		measure['loss'] 	/= len(data['test'].y)
		measure['accuracy'] /= len(data['test'].y)
		return measure

	def inform(epoch, status):
		message	= []
		message	+= [f'epoch:{epoch}/{config.iteration}']
		report(*message)
		for mode in ['train','valid','test']:
			message	 = [f'\t{mode:5}']
			message	+= [f"loss:{status[mode]['loss']: 18.7f}"]
			message	+= [f"accuracy:{status[mode]['accuracy']: 9.7f}"]
			report(*message)
		message	 = [f'\tunlab']
		message	+= [f"loss:{status['train']['loss-adversarial']: 18.7f}"]
		message	+= [f"accuracy:{status['train']['accuracy-unlabel']: 9.7f}"]
		report(*message)

	trails = []
	for epoch in range(config.iteration):
		status = {}
		status['valid'] = evaluate('valid')
		status['test']	= evaluate('test')
		status['train'] = update()
		if epoch%config.interval==0:
			inform(epoch, status)
		trails.append(status)
	return trails

	

# -------------------------------
import json
import argparse
def get_config():
	args = argparse.ArgumentParser()
	# general
	args.add_argument('--archive',						default='ARCHIVEs',		type=str)
	args.add_argument('--snapshot',			'-shot',	default='snapshot',		type=str)
	args.add_argument('--core',				'-core',	default=-1,				type=int)
	# data
	args.add_argument('--train_size',		'-trS',		default=2,			type=int)
	args.add_argument('--valid_size',		'-vaS',		default=3,			type=int)
	args.add_argument('--test_size',		'-teS',		default=5,			type=int)
	args.add_argument('--unlabeled_size',	'-unS',		default=7,			type=int)
	args.add_argument('--var',				'-var',		default=0,			type=float)
	args.add_argument('--data_seed',		'-Dseed',	default=1,			type=int)
	# architecture
	args.add_argument('--model_seed',		'-Mseed',	default=1,			type=int)
	args.add_argument('--hidden_dim',		'-hd',		default=11,			type=int)
	args.add_argument('--layer_size',		'-L',		default=1,			type=int)
	# optimizer
	args.add_argument('--iteration',		'-itr',		default=3,			type=int)
	args.add_argument('--interval',			'-ivl',		default=1,			type=int)
	args.add_argument('--lr',				'-lr',		default=0.01,		type=float)
	args.add_argument('--decay',			'-dcy',		default=0.01,		type=float)
	# VAT
	args.add_argument('--power_iter',		'-pwr',		default=1,			type=int)
	args.add_argument('--norm_eps',						default=1e-10,		type=float)
	args.add_argument('--epsilon',			'-eps',		default=0.1,		type=float)
	args.add_argument('--alpha',			'-alp',		default=1000.0,		type=float)
	args.add_argument('--xi',				'-xi',		default=0.01,		type=float)
	return args.parse_args()


# ------------------------------------------------------
import torch
def set_device(config):
	if 0<=config.core<torch.cuda.device_count() and torch.cuda.is_available():
		report(f'use GPU; core:{config.core}')
		torch.cuda.set_device(config.core)
	else:
		report('use CPU in this trial')
		config.core = -1
	print('-'*50)

# ------------------------------------------------------
import os
import json
import torch
import pickle
def main():
	report('start')

	config	= get_config()
	set_device(config)

	data	= get_data(config)
	model	= get_model(config)
	trails	= optimize(model, data, config)

	os.makedirs(config.snapshot, exist_ok=True)	
	with open(f'{config.snapshot}/config.json', 'w') as fp:
		json.dump(config.__dict__, fp, ensure_ascii=False, indent=4, sort_keys=True, separators=(',', ': '))
	with open(f'{config.snapshot}/trails.pkl', mode='wb') as fp:
		pickle.dump(trails,fp)

	os.makedirs(config.archive,	exist_ok=True)
	os.rename(config.snapshot,	f"{config.archive}/{datetime.datetime.now().strftime('%Y-%m-%d=%H-%M-%S-%f')}")	

	report('finish')



if __name__=="__main__":
	main()
