# coding: UTF-8
"""
パラメータ調整用
・この設定は特にlrの影響を調べる
・他のパラメータの影響も調べる？
	気が向けば
"""
# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import os
import time, random
def pworker(deq,core):
	time.sleep(core)
	while len(deq)!=0:
		config = deq.popleft()

		snapshot = f'snapshot-{core:02d}/'
		os.makedirs(snapshot,exist_ok=True)
		
		command  = ['nohup']
		command += [f"python -uO {config['script']}"]
		for key, value in config.items():
			if key!='script': command += [f'-{key} {value}']
		command += [f'-core {core}']		
		command += [f'-shot {snapshot}']
		command += [f'> {snapshot}/log.txt']
		command  = ' '.join(command) 

		report(f'remains:{len(deq)} core:{core} command:{command}')
		os.system(command)

# ------------------------------------------------------
# ------------------------------------------------------
import itertools
def get_parallel_comb(hypara:dict) ->list:
	configs = []
	keys = list(hypara.keys())
	for values in itertools.product(*[hypara[k] for k in keys]):
		config	= {key:value for key,value in zip(keys,values)}
		configs.append(config)
	return configs

import itertools
def get_mix(*lists) ->list:
	configs = []
	for parts in itertools.product(*lists):
		config = {}
		for p in parts:
			config.update(p)
		configs.append(config)
	return configs

# ------------------------------------------------------
def get_data_config() ->list:
	report('get data-config')

	hypara = {}
	hypara['Dseed']	= range(10)
	hypara['Mseed']	= range(10)

	hypara['trS']	= [2**2]
	hypara['vaS']	= [2**10]
	hypara['teS']	= [2**10]
	hypara['unS']	= [2**10]
	hypara['var']	= [0.4]

	configs = get_parallel_comb(hypara)

	print(f'data configs size:{len(configs)}')
	return configs

# ------------------------------------------------------
import copy
def get_model_config() ->list:
	report('get model-config')

	hypara = {}
	hypara['script']= ['experiment/main.py'] 
	# architecture
	hypara['hd']	= [100]
	hypara['L']		= [5]
	# iteration
	hypara['itr']	= [10001]
	hypara['ivl']	= [100]
	# virtual-adversarial
	hypara['xi']	= [1e-2]
	hypara['pwr']	= [3]
	hypara['eps']	= [1e-1]
	hypara['alp']	= [1e+4]
	common = get_parallel_comb(hypara)	

	serial = []
	for lr in [1e-1, 1e-2, 1e-3, 1e-4, 1e-5]:
		serial.append({'lr':lr, 'dcy':lr*10})
	
	configs = get_mix(common, serial)
	
	print(f'model configs size:{len(configs)}')
	return configs


def get_config() ->list:
	report('get-config')

	dconf = get_data_config()
	mconf = get_model_config()
	configs = get_mix(dconf, mconf)

	print(f'configs size:{len(configs)}')
	return configs

# ----------------------
from collections	import deque
from threading		import Thread
def main():
	print('-'*50)
	report('start')

	core_size = 6
	print(f'core size:{core_size}')

	configs	= get_config()
	deq 	= deque(configs)
	print('-'*50)

	pool = []
	for core in range(core_size):
		pool.append(Thread(target=pworker,args=(deq,2+core)))
	for p in pool:
		p.start()
	for p in pool:
		p.join()
	print('-'*50)

	report('finish')

if __name__ == '__main__':
	main()



# ----------------------
