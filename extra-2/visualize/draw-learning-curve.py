#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import math
def choose_accuracies(trails):
	best_valid_score	= -math.inf
	selected_test_score	= -math.inf
	for status in trails:
		v = status['valid']['accuracy']
		if v>best_valid_score:
			best_valid_score	= v
			selected_test_score	= status['test']['accuracy']
	best = selected_test_score
	last = trails[-1]['test']['accuracy']
	return {'Best':best, 'Last':last}

import os
import matplotlib.pyplot as plt
def view_logs(trails, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(8,4))
	plt.subplots_adjust(left=0.07, right=0.98, bottom=0.15, top=0.82, wspace=0.15, hspace=0.1)
	
	for i,quality in enumerate(['loss','accuracy']):
		ax = fig.add_subplot(1,2,1+i)
		for color,data in zip('bkr',['train','valid','test']):
			values = [status[data][quality] for status in trails]
			ax.plot(values, lw=0.5, color=color, label=data)

		ax.legend(framealpha=1.0)
		ax.grid()

		ax.set_title(quality)
		ax.set_xscale('log')
		ax.set_xlabel('epoch')
		if i==0: ax.set_yscale('log')
		if i==1: ax.set_ylim([0.0,1.005])

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.style.use('default');plt.clf();plt.cla();plt.close()

# -------------------------------------------------------------------
import os
import json
import pickle
from collections import defaultdict
def main():
	archive_dir = 'ARCHIVEs/'

	keywords = ['power_iter','epsilon','alpha','seed']

	bundle = defaultdict(list)
	for d in sorted(os.listdir(f'{archive_dir}')):
		if '.DS' in d: continue

		file_name = f'{archive_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)
		pwr,eps,alpha,seed = [config[k] for k in keywords]

		output_name = f'VIEW/Learning-Curve/{eps=:.0e}/{pwr=},{alpha=:.0e},{seed=}.png'
		if os.path.exists(output_name): continue

		file_name = f'{archive_dir}/{d}/trails.pkl'
		with open(file_name,'rb') as fp:
			trails = pickle.load(fp)
		if len(trails)<10: continue

		view_logs(trails, output_name)

# -------------------------------------------------------------------
if __name__=='__main__':
	main()






# -------------------------------------------------------------------
