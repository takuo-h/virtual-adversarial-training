#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import torch
import torch.nn as nn
import torch.tensor as tensor
import torch.nn.functional as F

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(6,4))
	plt.subplots_adjust(left=0.12, right=0.92, bottom=0.12, top=0.92, wspace=0, hspace=0)
	ax = fig.add_subplot(111)

	Seeds	= range(10)
	LRs		= [1e-1, 1e-2, 1e-3, 1e-4, 1e-5]

	for k,lr in enumerate(LRs):
		if lr not in buckets: continue
		accuracies = []
		for seed in Seeds:
			if seed in buckets[lr]:
				accuracies.append(buckets[lr][seed])
			else:
				accuracies.append(None)
		if all(v is None for v in accuracies): continue
		ax.plot(Seeds, accuracies, lw=0.3, color=cm.tab10(k), label=f'{lr:.0e}')
		ax.scatter(Seeds, accuracies, s=50, marker='x',lw=0.5, color=cm.tab10(k))

		accuracies = [v for v in accuracies if v is not None]
		average = np.mean(accuracies)
		ax.axhline(y=average, color=cm.tab10(k), lw=0.5,zorder=-3, linestyle='-')

	ax.legend(title=f'learning-rate',loc='lower center', ncol=5, handletextpad=0.3,labelspacing=0.5,columnspacing=0.8)

	ax.set_xlabel('model-seeds')
	ax.set_ylabel('accuracy')

	ax.set_ylim(0.5,1.0)

	ticks = Seeds
	for b in ticks:
		ax.axvline(x=b, color='gray',lw=0.5,zorder=-3, linestyle='-')
	ax.set_xticks(ticks)
	ax.set_xticklabels(map(str,ticks), fontsize=8)
	ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

	ax.minorticks_off()

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=60)
	plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
from collections import defaultdict
def main():
	archive_dir = 'ARCHIVEs/'

	buckets = {}
	for d in sorted(os.listdir(f'{archive_dir}')):
		if '.DS' in d: continue

		file_name = f'{archive_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)

		file_name = f'{archive_dir}/{d}/trails.pkl'
		with open(file_name,'rb') as fp:
			trails = pickle.load(fp)
		if len(trails)<10: continue

		best_valid_score 	= -math.inf
		selected_test_score	= -1
		for status in trails:
			valid	= status['valid']['accuracy']
			test	= status['test']['accuracy']
			if valid>best_valid_score:
				best_valid_score 	= valid
				selected_test_score	= test

		lr		= config['lr']
		Mseed	= config['model_seed']
		Dseed	= config['data_seed']
		key = (lr, Mseed, Dseed)
		buckets[key] = selected_test_score

	for Dseed in range(10):
		selected = defaultdict(dict)
		for key in buckets:
			lr,ms,ds = key
			if ds!=Dseed: continue
			selected[lr][ms] = buckets[key]
		if len(selected)==0: continue
		output_name = f'VIEW/WithSeeds/DataSeed={Dseed}.png'
		draw(selected, output_name)

# -------------------------------------------------------------------
if __name__=='__main__':
	main()


# -------------------------------------------------------------------
