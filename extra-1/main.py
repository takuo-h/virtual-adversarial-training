#!/usr/bin/env python
# coding: utf-8

# -------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------

import torch.nn.functional as F
#  # class方向にはsumする
def get_KLdiv(margin_q, margin_p):
	q		= F.softmax(margin_q, dim=1)
	logq	= F.log_softmax(margin_q, dim=1)
	logp	= F.log_softmax(margin_p, dim=1)
	div		= q*(logq-logp)
	return div.sum(dim=1)

from torch.autograd import Variable
def get_Radv(config, model, u, pu):
	d = torch.randn(u.shape)	# 正規分布のベクトルの生成
	if 0<=config.core: d = d.cuda(config.core)
	for _ in range(config.power_iter):
		d = config.xi * F.normalize(d, eps=config.norm_eps)
		d = Variable(d, requires_grad=True)
		padv = model(u + d)
		delta = get_KLdiv(pu, padv).mean(dim=0) # データ方向にaverageする

		model.zero_grad()
		delta.backward()
		d = d.grad.data.clone() # 上でVariableするのでOKなハズ

	d = F.normalize(d, eps=config.norm_eps)
	d = Variable(d, requires_grad=False)
	return config.epsilon * d


# ------------------------------------------------------

import numpy as np
from data import generate_data
from collections import namedtuple
def get_data(config):
	# generate x
	xL = generate_data('Lx', size=config.labeled_size, seed=config.seedL1, var=config.varL)
	xR = generate_data('Rx', size=config.labeled_size, seed=config.seedL2, var=config.varL)
	x = np.concatenate([xL,xR],axis=0)
	# generate y
	yL = np.zeros(config.labeled_size)
	yR = np.ones( config.labeled_size)
	y = np.concatenate([yL,yR],axis=0)
	# generate unlabeled x
	uL = generate_data('Lu', size=config.unlabeled_size, seed=config.seedU1, var=config.varU)
	uR = generate_data('Ru', size=config.unlabeled_size, seed=config.seedU2, var=config.varU)
	ux = np.concatenate([uL,uR],axis=0)
	# generate unlabelec y
	yL = np.zeros(config.unlabeled_size)
	yR = np.ones( config.unlabeled_size)
	uy = np.concatenate([yL,yR],axis=0)

	# convert into torch
	x  = torch.tensor(x, dtype=torch.float)
	y  = torch.tensor(y, dtype=torch.long)
	ux = torch.tensor(ux, dtype=torch.float)
	uy = torch.tensor(uy, dtype=torch.long)

	if 0<=config.core:
		x = x.cuda()
		y = y.cuda()
		ux = ux.cuda()
		uy = uy.cuda()

	return namedtuple('DataSet', ('x y ux uy'))(x=x, y=y, ux=ux, uy=uy)

# -------------------------------
import random
import torch.nn as nn
class Model(nn.Module):
	def __init__(self, hidden_dim, layer_size):
		super(Model, self).__init__()
		d,L = hidden_dim, layer_size
		self.Li = nn.Linear(2, d)
		self.Ls	= nn.ModuleList([nn.Linear(d,d) for _ in range(L)])
		self.Lo = nn.Linear(d, 2)

	def forward(self, x):
		x = self.Li(x)
		for L in self.Ls:
			x = torch.nn.functional.relu(L(x))
		x = self.Lo(x)
		return x

def get_model(config):
	random.seed(0)
	np.random.seed(0)
	torch.manual_seed(0)
	model = Model(config.hidden_dim, config.layer_size)
	return model

# ------------------------------------------------------
import torch.optim as optim
def optimize(model, data, config):
	optimizer	= optim.Adam(model.parameters(), lr=config.lr)
	get_loss	= torch.nn.CrossEntropyLoss()
	scheduler	= lambda epoch: 1 / ((epoch-1)*config.decay + 1)
	scheduler	= optim.lr_scheduler.LambdaLR(optimizer, lr_lambda = scheduler)

	def update():
		# standard loss
		px 		= model(data.x)
		lloss	= get_loss(px, data.y)

		# vat
		up		= model(data.ux)
		d		= get_Radv(config, model, data.ux, up.detach())
		ap		= model(data.ux + d)
		vadv	= get_KLdiv(up, ap)
		advloss	= vadv.mean(dim=0)

		loss = lloss + config.alpha * advloss

		optimizer.zero_grad()
		loss.backward()
		optimizer.step()
		scheduler.step()

		status = {}
		status['standard-loss']		= lloss.item()
		status['adversarial-loss']	= advloss.item()
		status['total-loss']		= loss.item()
		status['unlabeled-loss']	= get_loss(up, data.uy).item()
		status['uadv-value'] 	= vadv.detach().cpu().numpy().tolist()
		status['upred-value']	= up.detach().cpu().numpy().tolist()

		return status

	def inform(epoch, status):
		message  = []
		message += [f'epoch:{epoch:03d}/{config.iteration}']
		report(*message)
		message  = [f'\tloss']
		message += [f"standard :{status['standard-loss']:7.5f}"]
		message += [f"adv  :{status['adversarial-loss']:7.5f}"]
		report(*message)
		message  = [f'\tloss']
		message += [f"unlabeled:{status['unlabeled-loss']:7.5f}"]
		message += [f"total:{status['total-loss']:7.5f}"]
		report(*message)

	trails = []
	model.train()
	for epoch in range(config.iteration):
		status = update()
		if epoch%config.interval==0:
			inform(epoch, status)
		trails.append(status)
	return trails



# -------------------------------
import json
import argparse
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--core',					'-core',	default=-1,			type=int)

	# settings for data
	args.add_argument('--labeled_size',			'-numX',	default=2**2,		type=int) # cifat10だと4000?
	args.add_argument('--unlabeled_size',		'-numU',	default=2**10,		type=int)

	args.add_argument('--seedL1',				'-sL1',		default=23,			type=int)
	args.add_argument('--seedL2',				'-sL2',		default=7,			type=int)
	args.add_argument('--seedU1',				'-sU1',		default=0,			type=int)
	args.add_argument('--seedU2',				'-sU2',		default=1,			type=int)
	args.add_argument('--varL',					'-varL',	default=0.1,		type=float)
	args.add_argument('--varU',					'-varU',	default=0.4,		type=float)

	# settings for architecture
	args.add_argument('--hidden_dim',			'-hd',		default=100,		type=int)
	args.add_argument('--layer_size',			'-L',		default=5,			type=int)

	# settings for optimizer
	args.add_argument('--iteration',			'-itr',		default=30001,		type=int)
	args.add_argument('--interval',				'-ivl',		default=100,		type=int)
	args.add_argument('--lr',					'-lr',		default=0.01,		type=float)
	args.add_argument('--decay',				'-dcy',		default=0.01,		type=float)

	# settings for VAT
	args.add_argument('--power_iter',			'-pwr',		default=1,			type=int)
	args.add_argument('--norm_eps',							default=1e-10,		type=float)
	args.add_argument('--epsilon',				'-eps',		default=0.1,		type=float)
	args.add_argument('--alpha',				'-alp',		default=1000.0,		type=float)
	args.add_argument('--xi',					'-xi',		default=0.01,		type=float)

	return args.parse_args()


# ------------------------------------------------------
import torch
def set_device(config):
	if 0<=config.core<torch.cuda.device_count() and torch.cuda.is_available():
		report(f'use GPU; core:{config.core}')
		torch.cuda.set_device(config.core)
	else:
		report('use CPU in this trial')
		config.core = -1
	print('-'*50)

# ------------------------------------------------------
import os
import json
import torch
import pickle
def main():
	report('start')

	config	= get_config()
	set_device(config)

	data	= get_data(config)
	model	= get_model(config)

	records = {}
	records['x']	 = data.x.numpy()
	records['y']	 = data.y.numpy()
	records['ux']	 = data.ux.numpy()
	records['uy']	 = data.uy.numpy()
	records['trails']= optimize(model, data, config)

	os.makedirs('OUTPUTs',exist_ok=True)
	with open(f'OUTPUTs/records.pkl', 'wb') as fp:
		pickle.dump(records, fp)
	with open(f'OUTPUTs/config.json', 'w') as fp:
		json.dump(config.__dict__, fp, ensure_ascii=False, indent=4, sort_keys=True, separators=(',', ': '))
	
	report('finished')
	print('-'*50)


if __name__=="__main__":
	main()
