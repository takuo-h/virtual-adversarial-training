#!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def draw(data, status, extreme, epoch, file_name):
	fig = plt.figure(figsize=(9,2.5))
	plt.subplots_adjust(left=0, right=1, top=0.8, bottom=0, wspace=0.1, hspace=0.0)

	for i,key in enumerate(['upred-value','uadv-value','upred-value']):
		value = status[key]

		ax = fig.add_subplot(1, 3, i+1)
		ax.axis('equal')

		x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==0]
		x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==0]
		ax.scatter(x0, x1, s=3, color=cm.tab10(1), zorder=10, marker='d', label='class=1') # labeled data

		x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==1]
		x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==1]
		ax.scatter(x0, x1, s=3, color=cm.tab10(2), zorder=10, marker='d', label='class=0') # labeled data

		if i==0:
			ax.set_title('Score Assigned to Class:0')
			color = cm.Reds([max(v2-v1,0) for v1,v2 in value])
			lw = 0.05
		if i==1:
			ax.set_title('Local Distributional Smoothness')
			value = [max(v,0) for v in value]
			min_value, max_value = min(value), max(value)
			width = max(max_value,0) - max(min_value,0)
			color = [(v-min_value)/width for v in value]
			color = cm.Blues(color)
			lw = 0.03
		if i==2:
			ax.set_title('Score Assigned to Class:1')
			color = cm.Greens([max(v1-v2,0) for v1,v2 in value])
			lw = 0.05

		ax.scatter(data.ux[:,0], data.ux[:,1], s=10, color=color, edgecolor='black', linewidth=lw,  zorder=0)
		ax.scatter(data.ux[0,0], data.ux[0,1], s=20, color='none', edgecolor='none', linewidth=0.5, label='unlabel', zorder=-1)
		
		ax.legend(fontsize=9,framealpha=1.0).set_zorder(-1)

		ax.get_xaxis().set_ticks([])
		ax.get_yaxis().set_ticks([])

		ax.axis('off')
	plt.suptitle(f'epoch:{epoch}')

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()



# ------------------------------------------------------
import math
from collections import namedtuple
def get_extreme_pred(trails):
	max_value = -math.inf
	min_value = +math.inf
	for status in trails:
		diff = [v2-v1 for v1,v2 in status['upred-value']]
		max_value = max(max_value, max(diff))
		min_value = min(min_value, min(diff))
	return namedtuple('_','min max')(min=min_value,max=max_value)

# -------------------------------
import argparse
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--span',	'-span', default=100, type=int)
	return args.parse_args()

# ------------------------------------------------------
import pickle
def main():
	report('start')

	config = get_config()

	with open('OUTPUTs/records.pkl', 'rb') as fp:
		records	= pickle.load(fp)

	data = namedtuple('DataSet', ('x y ux uy'))
	data = data(**{k:records[k] for k in 'x y ux uy'.split()})

	extreme = get_extreme_pred(records['trails'])
	for epoch, status in enumerate(records['trails']):
		if epoch%config.span==0:
			draw(data, status, extreme, epoch, f'OUTPUTs/snapshot/{epoch}.png')

	report('finish')
	print('-'*50)


# ------------------------------------------------------
if __name__=='__main__':
	main()





#