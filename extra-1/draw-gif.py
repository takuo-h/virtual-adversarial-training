#!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import os
from PIL import Image
def main():
	report('start')

	snapshot_dir = 'OUTPUTs/snapshot/'
	files = {}
	for f in os.listdir(snapshot_dir):
		if '.DS' in f: continue
		epoch = int(f.replace('.png',''))
		files[epoch] = f'{snapshot_dir}/{f}'

	images = []
	for epoch,f in sorted(files.items(),key=lambda x:x[0]):
		images.append(Image.open(f))
	
	images[0].save('OUTPUTs/replica.gif', save_all=True, append_images=images[1:], duration=400, loop=0)

	report('finish')

if __name__=='__main__':
	main()