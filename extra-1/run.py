#!/usr/bin/env python
# coding: utf-8

import os
import datetime
def carryout(var, itr, decay, span):
	output_dir = 'OUTPUTs'

	os.system(f'mkdir -p {output_dir}')
	os.system(f'python -u main.py -varU {var} -itr {itr} -dcy {decay} > {output_dir}/main.log')
	os.system(f'python -u draw-snapshot.py -span {span} > {output_dir}/draw-snapshot.log')
	os.system(f'python -u draw-gif.py > {output_dir}/draw-gif.log')

	file_name = f"ARCHIVEs/var={var},decay={decay}"
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	os.rename(output_dir, file_name)
	
def main():
	carryout(var=0.1, itr=2001, decay=0.001, span=100)
	carryout(var=0.1, itr=2001, decay=0.010, span=100)
	carryout(var=0.4, itr=5001, decay=0.010, span=250)
	
if __name__=='__main__':
	main()