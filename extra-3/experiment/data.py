#!/usr/bin/env python
# coding: utf-8

# -------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import numpy as np
def _sample(mode, size, var):
	if mode==0: shift = 0
	if mode==1: shift = np.pi
	angle = np.random.uniform(0.05,1,size=(size,1))
	angle = angle*2.5*np.pi
	x1 = np.cos(angle+shift)*angle
	x2 = np.sin(angle+shift)*angle
	x = np.concatenate([x1,x2],axis=1)
	return x+np.random.normal(0,var,size=x.shape)

import torch
from collections import namedtuple
def generate(size, seed, var, core):
	np.random.seed(seed)
	x = np.concatenate([_sample(i,size,var) for i in [0,1]],axis=0)
	x = torch.tensor(x, dtype=torch.float)
	y = torch.tensor([0]*size+[1]*size, dtype=torch.long)
	if 0<=core:
		x = x.cuda()
		y = y.cuda()
	return namedtuple('_', ('x y'))(x=x,y=y)

# ------------------------------------------------------
def unit_test():
	pass

if __name__=="__main__":
	unit_test()
