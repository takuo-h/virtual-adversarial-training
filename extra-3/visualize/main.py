#!/usr/bin/env python
# coding: utf-8

import os
def main():
	os.system('python visualize/draw-heatmap.py')
	os.system('python visualize/draw-size.py')
	os.system('python visualize/draw-trend.py')

if __name__=="__main__":
	main()
