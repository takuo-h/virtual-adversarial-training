#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import torch
import torch.nn as nn
import torch.tensor as tensor
import torch.nn.functional as F

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(5,3.5))
	plt.subplots_adjust(left=0.13, right=0.97, bottom=0.13, top=0.97, wspace=0, hspace=0)
	ax = fig.add_subplot(111)

	sizes = np.array(sorted(buckets))
	accuracies = [buckets[k] for k in sizes]
	var		= [np.var(v)**0.5 for v in accuracies]
	mean	= [np.mean(v) for v in accuracies]
	ax.plot(sizes, mean, color='black')

	for i,size in enumerate(sizes):
		y = sorted(buckets[size])
		L = len(y)
		x = [(i-L//2)/(2*L) for i in range(L)]
		x = [2**v for v in x]
		x = [size*v for v in x]
		c = [cm.seismic(float(i)/L) for i in range(L)]
		ax.scatter(x,y,c=c,marker='s',s=3.0, zorder=-1)

	ax.set_xlabel('Unlabeled Data Size',fontsize=12)
	ax.set_ylabel('Accuracy',fontsize=12)
	ax.set_xscale('log')

	ax.set_ylim(0.5,1.0)
	ax.set_xlim(16/1.5,1024*1.5)

	ticks = [2**v for v in range(4,11)]
	for b in ticks:
		ax.axvline(x=b, color='gray',lw=0.5,zorder=-3, linestyle='-')
	ax.set_xticks(ticks)
	ax.set_xticklabels(map(str,ticks), fontsize=8)
	ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

	ax.minorticks_off()

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=80)
	plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
from collections import defaultdict
def main():
	report('start draw-trend')
	archive_dir = 'ARCHIVEs/'

	buckets = defaultdict(list)
	for d in sorted(os.listdir(f'{archive_dir}')):
		if '.DS' in d: continue

		file_name = f'{archive_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)

		file_name = f'{archive_dir}/{d}/trails.pkl'
		with open(file_name,'rb') as fp:
			trails = pickle.load(fp)
		if len(trails)<10: continue

		best_valid_score 	= -math.inf
		selected_test_score	= -1
		for status in trails:
			valid	= status['valid']['accuracy']
			test	= status['test']['accuracy']
			if valid>best_valid_score:
				best_valid_score 	= valid
				selected_test_score	= test

		unl		= config['unlabeled_size']
		buckets[unl].append(selected_test_score)

	output_name = f'VIEW/Trend.png'
	draw(buckets, output_name)

	report('finish')
	print('-'*50)

# -------------------------------------------------------------------
if __name__=='__main__':
	main()
