#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import torch
import torch.nn as nn
import torch.tensor as tensor
import torch.nn.functional as F

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(buckets, file_name, title):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(5,3.5))
	plt.subplots_adjust(left=0.12, right=0.97, bottom=0.15, top=0.92, wspace=0, hspace=0)
	ax = fig.add_subplot(111)

	Seeds	= range(10)


	mean = []
	for i,ms in enumerate(Seeds):
		accuracies = []
		for j,ds in enumerate(Seeds):
			key = (ms,ds)
			if key in buckets:
				accuracies.append(buckets[key])
		if len(accuracies)==0: continue
		ax.scatter([ms]*len(accuracies), accuracies, s=70, color='black', marker='x', lw=0.6)
		mean.append(np.mean(accuracies))

	x = Seeds[:len(mean)]
	y = mean
	color = cm.viridis(np.arange(10)/10)
	ax.bar(x, y, color='w', alpha=1.0, zorder=-2)
	ax.bar(x, y, color=color, alpha=0.85, zorder=-1)

	ax.set_xlabel('Model Seed',fontsize=13)
	ax.set_ylabel('Accuracy',fontsize=13)

	ax.set_ylim(0.5,1.0)

	ticks = Seeds
	for b in ticks:
		ax.axvline(x=b, color='gray',lw=0.5,zorder=-3, linestyle='-')
	ax.set_xticks(ticks)
	ax.set_xticklabels(map(str,ticks), fontsize=8)
	ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

	ax.minorticks_off()

	ax.set_title(title,fontsize=13)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=80)
	plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
from collections import defaultdict
def main():
	report('start draw-size')
	archive_dir = 'ARCHIVEs/'

	buckets = defaultdict(dict)
	for d in sorted(os.listdir(f'{archive_dir}')):
		if '.DS' in d: continue

		file_name = f'{archive_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)

		file_name = f'{archive_dir}/{d}/trails.pkl'
		with open(file_name,'rb') as fp:
			trails = pickle.load(fp)
		if len(trails)<10: continue

		best_valid_score 	= -math.inf
		selected_test_score	= -1
		for status in trails:
			valid	= status['valid']['accuracy']
			test	= status['test']['accuracy']
			if valid>best_valid_score:
				best_valid_score 	= valid
				selected_test_score	= test

		unl		= config['unlabeled_size']
		Mseed	= config['model_seed']
		Dseed	= config['data_seed']
		key = (Mseed, Dseed)
		buckets[unl][key] = selected_test_score

	for unl in buckets:
		if len(buckets[unl])==0: continue
		output_name = f'VIEW/WithSize/unlabeled-size={unl}.png'
		draw(buckets[unl], output_name, f'Unlabeled Data Size:{unl}')

	report('finish')
	print('-'*50)

# -------------------------------------------------------------------
if __name__=='__main__':
	main()

