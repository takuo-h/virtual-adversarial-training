#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import torch
import torch.nn as nn
import torch.tensor as tensor
import torch.nn.functional as F

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(buckets, file_name, title):
	print(f'draw:{file_name}')

	fig = plt.figure(figsize=(6,6))
	plt.subplots_adjust(left=0.09, right=0.91, bottom=0.09, top=0.91)
	ax = fig.add_subplot(111)

	data_seeds	= range(10)
	model_seeds	= range(10)
	
	values = []
	for ds in data_seeds:
		row = []
		for ms in model_seeds:
			key = (ms,ds) 
			if key in buckets:
				row.append(buckets[key]) 
			else:
				row.append(0)
		values.append(row)

	ax.imshow(values, cmap='hot', vmin=0.5, vmax=1)

	ax.set_xlabel('Model Seed',fontsize=30)
	ax.set_ylabel('Data Seed',fontsize=30)
	ax.set_title(title,fontsize=30)
		
	ax.set_xticks([])
	ax.set_xticklabels([], fontsize=8)

	ax.set_yticks([])
	ax.set_yticklabels([], fontsize=8)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=30)
	plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
from collections import defaultdict
def main():
	report('start draw-heatmap')
	archive_dir = 'ARCHIVEs/'

	buckets = defaultdict(dict)
	for d in sorted(os.listdir(f'{archive_dir}')):
		if '.DS' in d: continue

		file_name = f'{archive_dir}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)

		file_name = f'{archive_dir}/{d}/trails.pkl'
		with open(file_name,'rb') as fp:
			trails = pickle.load(fp)
		if len(trails)<10: continue

		best_valid_score 	= -math.inf
		selected_test_score	= -1
		for status in trails:
			valid	= status['valid']['accuracy']
			test	= status['test']['accuracy']
			if valid>best_valid_score:
				best_valid_score 	= valid
				selected_test_score	= test

		unl		= config['unlabeled_size']
		Mseed	= config['model_seed']
		Dseed	= config['data_seed']
		key = (Mseed, Dseed)
		buckets[unl][key] = selected_test_score

	for unl in buckets:
		if len(buckets[unl])==0: continue
		output_name = f'VIEW/HeatMap/unlabeled-size={unl}.png'
		draw(buckets[unl], output_name, f'Unlabeled Data Size:{unl}')

	report('finish')
	print('-'*50)

# -------------------------------------------------------------------
if __name__=='__main__':
	main()

