# 1-概要

- 次の論文の再実装等をした \
	[Distributional Smoothing with Virtual Adversarial Training](https://arxiv.org/abs/1507.00677) \
	全実験はrun.py or run.shを実行することで再現可能

- この論文は何？ \
	ラベルなしデータをいい感じに活用する手法を提案

- 技術的には何をしてる？ 
	- 与えられたデータとモデルに対し、Local Distributional Smoothing(LDS)という量を考える．\
	これは、データに悪意的な摂動を加えた時、予測がどの程度影響を受けるかを計る
	- Hessianの主固有ベクトルはこのLDSを最大化する \
	power-method+有限差分法を用れば、Back-Prop的な方法で効率的に近似できる
	- LDSはKLに基づくため、ラベルなしデータにも適用可能 \
	分離平面がデータ分布を横断しないような正則化として活用できる


# 2-実験


#### 実験:Replciation
- 参照した挙動は著者のgithubの[gif](https://raw.githubusercontent.com/takerum/vat_tf/master/vat.gif)による．
- 1枚の図の見方
  - 円の大きさは、Local Distributional Smoothnessの値に由来する．
- 結果
  - 基本的に大きな円が青/赤の境界に出現している
  - 学習が進むとそれが外に追いやられている
  - LDSの値が大きい領域とデータの境界(今の場合なら中間のsin波状の空間)が一致する(分離平面がデータ分布を横断しない)ように学習してることを意味する

再現したい挙動 | 再現したの(1枚に表示) |
-------|---------------|
<img src="replication/target-behavior/vat.gif"  width="240" height="120"> | <img src="replication/OUTPUTs/1-film.gif"  width="180" height="120"> | 

--------------------------------------

#### 実験:Extra-1
- より難しいデータでも扱えるか試してみた
  - 渦データの回転数を増やした
  - label付きデータは、◇で表示した4点(中心のオレンジ/緑色)
- 結果
  - 調節を頑張ればスイスロール(?)も扱えるが、ハイパラに結構センシティブっぽい
  - 例えば例1と事例2の違いは、僅かな学習率の差．ただし学習率は$`\text{lr}/(1+\text{epoch}*\text{decay})`$で決定される
  - 特定のクラスに全部染められると、データ横断的な分離平面ではないので、LDSが機能しないっぽい．


詳細 (var=データの分散, lr,dacy=学習率関係) | fittingの過程 |
-------|-------|
事例-1: var=$`0.1`$,　lr=$`0.01`$, decay=$`0.001`$ | <img src="extra-1/OUTPUTs/var=0.1,decay=0.001/replica.gif"  width="360" height="120"> | 
事例-2: var=$`0.1`$,　lr=$`0.01`$, decay=$`0.01`$ | <img src="extra-1/OUTPUTs/var=0.1,decay=0.01/replica.gif"  width="360" height="120"> | 
事例-3: var=$`0.4`$,　lr=$`0.01`$, decay=$`0.01`$ | <img src="extra-1/OUTPUTs/var=0.4,decay=0.01/replica.gif"  width="360" height="120"> | 

--------------------------------------

#### 実験:Extra-2
- modelのseedやdataのseedに結構影響される？
- ちゃんと調整すればそうでもない
  - model-seedを10個、data-seedを10回変えてやった
  - 各セルは精度を表す
  - 色はcolormapはheat, min=0.5, max=1.0
- 諸々の改善は気が向いたら

heatmap |
-------|
<img src="extra-2/VIEW/HeatMap/lr=0.001.png"  width="120" height="120"> |

--------------------------------------
#### 実験:Extra-3
- unlabeledデータの数を変えたらどうなるか
  - 少なくなると、シードの影響が大きくなりそう

--------------------------------------

--------------------------------------
