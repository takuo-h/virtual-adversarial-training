# coding: UTF-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def draw(data, status, epoch, file_name):
	fig = plt.figure(figsize=(9,2.5))
	plt.subplots_adjust(left=0, right=1, top=0.8, bottom=0, wspace=0.1, hspace=0.0)

	for i,key in enumerate(['upred','uloss','upred']):
		value = status[key]

		ax = fig.add_subplot(1, 3, i+1)
		ax.axis('equal')

		x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==0]
		x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==0]
		ax.scatter(x0, x1, s=40, color=cm.tab10(1), zorder=10, marker='d', label='class=1') # labeled data

		x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==1]
		x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==1]
		ax.scatter(x0, x1, s=40, color=cm.tab10(2), zorder=10, marker='d', label='class=0') # labeled data

		# draw unlabeled data
		if i==0:
			ax.set_title('Score Assigned to Class:0')
			color = cm.Reds([min(max(v2-v1,0),1)*0.8 for v1,v2 in value])
		if i==1:
			ax.set_title('Local Distributional Smoothness')
			color = cm.Greens([v for v in value])
		if i==2:
			ax.set_title('Score Assigned to Class:1')
			color = cm.Blues([min(max(v1-v2,0),1)*0.8 for v1,v2 in value])

		ax.scatter(data.u[:,0], data.u[:,1], s=15, color=color, edgecolor='black', linewidth=0.3, zorder=0)
		ax.scatter(data.u[0,0], data.u[0,1], s=15, color='white', edgecolor='black', linewidth=0.3, label='unlabel', zorder=-1)

		ax.legend(fontsize=9,framealpha=1.0)

		ax.get_xaxis().set_ticks([])
		ax.get_yaxis().set_ticks([])

		ax.axis('off')
	plt.suptitle(f'epoch:{epoch}')

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()


# ------------------------------------------------------
import math
import pickle
from collections import namedtuple
def main():
	report('start')
	with open(f'OUTPUTs/records.pkl', 'rb') as fp:
		records = pickle.load(fp)

	data = namedtuple('DataSet', ('x y u'))
	data = data(**{k:records[k] for k in 'xyu'})

	for epoch, status in enumerate(records['trails']):
		report(f'\tepoch:{epoch}')
		draw(data, status, epoch, f'OUTPUTs/state3/{epoch}.png')

	report('finish')
	print('-'*50)

# ------------------------------------------------------
if __name__=='__main__':
	main()





#