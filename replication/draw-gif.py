# coding: UTF-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------

import os
from PIL import Image
def main():
	report('start')

	for data_dir, file_name in zip(['state1','state3'],['1-film','3-film']):
		snapshot_dir = f'OUTPUTs/{data_dir}/'
		files = {}
		for f in os.listdir(snapshot_dir):
			if '.DS' in f: continue
			epoch = int(f.replace('.png',''))
			if epoch%10==0:
				files[epoch] = f'{snapshot_dir}/{f}'

		images = []
		for epoch,f in sorted(files.items(),key=lambda x:x[0]):
			images.append(Image.open(f))
		
		images[0].save(f'OUTPUTs/{file_name}.gif', save_all=True, append_images=images[1:], duration=400, loop=0)

	report('finish')
	print('-'*50)

if __name__=='__main__':
	main()