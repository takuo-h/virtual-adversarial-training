#!/usr/bin/env python
# coding: utf-8

# -------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)))

# ------------------------------------------------------

import torch.nn.functional as F
def get_KLdiv(margin_q, margin_p):
	q		= F.softmax(margin_q, dim=1)
	logq	= F.log_softmax(margin_q, dim=1)
	logp	= F.log_softmax(margin_p, dim=1)
	div		= q*(logq-logp)
	return div.sum(dim=1)

from torch.autograd import Variable
def get_Radv(config, model, u, pu):
	d = torch.randn(u.shape)	# 正規分布のベクトルの生成
	for _ in range(config.power_iter):
		d = config.xi * F.normalize(d, eps=config.norm_eps)
		d = Variable(d, requires_grad=True)
		padv = model(u + d)
		delta = get_KLdiv(pu, padv).mean(dim=0)

		model.zero_grad()
		delta.backward()
		d = d.grad.data.clone().cpu()

	d = F.normalize(d, eps=config.norm_eps)
	d = Variable(d, requires_grad=False)
	return config.epsilon * d

# -------------------------------
import torch
import random
import numpy as np
class Model(torch.nn.Module):
	def __init__(self, hidden_dim=10, hidden_layer=4):
		super(Model, self).__init__()
		d, L = hidden_dim, hidden_layer
		self.Li	= torch.nn.Linear(2, d)
		self.Ls	= torch.nn.ModuleList([torch.nn.Linear(d,d)	for _ in range(L)])
		self.Lo = torch.nn.Linear(d, 2)

	def __call__(self, x):
		x = self.Li(x)
		for L in self.Ls:
			x = torch.relu(L(x))+x
		x = self.Lo(x)
		return x

def get_model(config,seed=0):
	random.seed(seed)
	np.random.seed(seed)
	torch.manual_seed(seed)
	return Model()

# ------------------------------------------------------
import numpy as np
from data import generate_data
from collections import namedtuple
def get_data(config):
	xL = generate_data('Left',	config.labeled_size, seed=56)
	xR = generate_data('Right',	config.labeled_size, seed=18)
	x = np.concatenate([xL,xR],axis=0)
	x = torch.tensor(x, dtype=torch.float)

	yL = np.zeros(config.labeled_size)
	yR = np.ones( config.labeled_size)
	y = np.concatenate([yL,yR],axis=0)
	y = torch.tensor(y, dtype=torch.long)
 
	uL = generate_data('Left',	config.unlabeled_size, seed=0)
	uR = generate_data('Right',	config.unlabeled_size, seed=1)
	u = np.concatenate([uL,uR],axis=0)
	u = torch.tensor(u, dtype=torch.float)

	return namedtuple('DataSet', 'x y u')(x=x, y=y, u=u)

# -------------------------------
import argparse
def get_config():
	args = argparse.ArgumentParser()
	args.add_argument('--output_dir',		default='OUTPUTs/data',	type=str)

	args.add_argument('--labeled_size',		default=2**2,			type=int) # cifat10だと4000?
	args.add_argument('--unlabeled_size',	default=2**10,			type=int)

	args.add_argument('--iteration',		default=100,			type=int)
	args.add_argument('--lr',				default=0.1,			type=float)
	args.add_argument('--power_iter',		default=10,				type=int)
	args.add_argument('--norm_eps',			default=1e-10,			type=float)
	args.add_argument('--epsilon',			default=0.1,			type=float)
	args.add_argument('--xi',				default=1e-4,			type=float)
	return args.parse_args()

# ------------------------------------------------------
def optimize(model, data, config):
	optimizer	= torch.optim.Adam(model.parameters(), lr=config.lr)
	get_loss	= torch.nn.CrossEntropyLoss()

	trails = []
	for epoch in range(config.iteration):
		# standard loss
		loss	= get_loss(model(data.x), data.y)
		# vat
		pu		= model(data.u)
		ppu		= pu.detach()
		Radv	= get_Radv(config, model, data.u, ppu)
		padv	= model(data.u + Radv.detach())
		vadv	= get_KLdiv(pu, padv)
		loss	+= vadv.mean(dim=0)

		optimizer.zero_grad()
		loss.backward()
		optimizer.step()

		report(f'epoch:{epoch} loss:{loss.item()}')

		status = {}
		status['uloss'] = vadv.detach().cpu().numpy().tolist()
		status['upred'] = pu.detach().cpu().numpy().tolist()
		trails.append(status)
	return trails

# ------------------------------------------------------
import os
import torch
import pickle
def main():
	report('start')

	config	= get_config()
	data	= get_data(config)
	model	= get_model(config)

	records = {}
	records['x']	 = data.x.numpy()
	records['y']	 = data.y.numpy()
	records['u']	 = data.u.numpy()
	records['trails'] = optimize(model, data, config)

	file_name = f'OUTPUTs/records.pkl'
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	with open(file_name, 'wb') as fp:
		pickle.dump(records, fp)

	report('finished')
	print('-'*50)

if __name__=="__main__":
	main()
