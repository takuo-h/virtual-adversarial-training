# coding: UTF-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import numpy as np
def get_circleL(size):
	angle = np.random.uniform(0.45,1.05,size=(size,1))
	angle = angle*2*np.pi
	x1 = np.cos(angle)-0.6
	x2 = np.sin(angle)
	x = np.concatenate([x1,x2],axis=1)
	x = x+np.random.normal(0,0.15,size=x.shape)
	return x

def get_circleR(size):
	angle = np.random.uniform(-0.05,0.55,size=(size,1))
	angle = angle*2*np.pi
	x1 = np.cos(angle)+0.6
	x2 = np.sin(angle)
	x = np.concatenate([x1,x2],axis=1)
	x = x+np.random.normal(0,0.15,size=x.shape)
	return x

# ------------------------------------------------------

import math
import random
import numpy as np
def generate_data(dist, size=100, seed=0):
	if seed==-1: seed = random.randint(0,2**31)
	random.seed(seed)
	np.random.seed(seed)

	if dist=='Left':	return get_circleL(size)
	if dist=='Right':	return get_circleR(size)

	assert False, (f'no such distribution:{dist}')


# ------------------------------------------------------
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def test():
	file_name = f'OUTPUTs/sample-full-labeled.png'
	num_sample = 2**10

	fig = plt.figure(figsize=(6,6))
	plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

	ax = fig.add_subplot(1, 1, 1)
	ax.axis('equal')

	for i,dist in enumerate(['Left','Right']):
		x = generate_data(dist, num_sample)
		ax.scatter(x[:,0], x[:,1], s=0.5, color=cm.tab10(i), label=f'class:{dist}')
	ax.legend()

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)


# ------------------------------------------------------
if __name__=='__main__':
	test()





#