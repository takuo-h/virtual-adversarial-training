# coding: UTF-8

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%E:%S')+' '+' '.join(map(str,args)))

# ------------------------------------------------------
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
def draw(data, status, epoch, file_name):
	fig = plt.figure(figsize=(4,2.5))
	plt.subplots_adjust(left=0, right=1, top=1.0, bottom=0, wspace=0.1, hspace=0.0)

	upred = status['upred']
	uloss = status['uloss']

	ax = fig.add_subplot(1,1,1)
	ax.axis('equal')

	x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==0]
	x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==0]
	ax.scatter(x0, x1, s=40, color=cm.tab10(1), zorder=10, marker='d', label='class=1') # labeled data

	x0 = [data.x[j,0] for j,yy in enumerate(data.y) if yy==1]
	x1 = [data.x[j,1] for j,yy in enumerate(data.y) if yy==1]
	ax.scatter(x0, x1, s=40, color=cm.tab10(2), zorder=10, marker='d', label='class=0') # labeled data

	ax.scatter(data.u[0,0], data.u[0,1], s=40, color='white', edgecolor='black', linewidth=0.5, label='unlabel', zorder=-1)
	ax.scatter(data.u[0,0], data.u[0,1], s=50, color='white', edgecolor='none', linewidth=0.3, zorder=0)

	color = []
	for v in [v1-v2 for v1,v2 in upred]:
		if v<0:	color.append(cm.Reds(min(-v,1)*0.8))
		else:	color.append(cm.Blues(min(v,1)*0.8))

	scale = [5+128*math.log(v+1) for v in uloss]
	ax.scatter(data.u[:,0], data.u[:,1], s=scale, color=color, edgecolor='none', zorder=1)

	ax.legend(fontsize=9,framealpha=1.0)

	ax.get_xaxis().set_ticks([])
	ax.get_yaxis().set_ticks([])

	ax.axis('off')
	ax.set_title(f'epoch:{epoch: 3d}',y=0.88)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()


# ------------------------------------------------------
import math
import pickle
from collections import namedtuple
def main():
	report('start')
	with open(f'OUTPUTs/records.pkl', 'rb') as fp:
		records = pickle.load(fp)

	data = namedtuple('DataSet', ('x y u'))
	data = data(**{k:records[k] for k in 'xyu'})

	for epoch, status in enumerate(records['trails']):
		report(f'\tepoch:{epoch}')
		draw(data, status, epoch, f'OUTPUTs/state1/{epoch}.png')

	report('finish')
	print('-'*50)

# ------------------------------------------------------
if __name__=='__main__':
	main()





#